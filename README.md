﻿# Studenti, kteří splnili požadavky k zápočtu

| Jméno        | Příjmení           | Osobní číslo  |
| ------------- |-------------| -----|
| Dan      | Charousek | F15001 |
| Marek    | Beran     | ST75216 |
| Jiří     | Machovec  | F16057 |
| Jan | Zaloudek | F17031 |
